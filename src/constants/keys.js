const databaseEncryptionKey =
	process.env.CONNECT_ENCRYPTION_KEY || `l9JPaMZnFO4qqaExrpTlLflVe4QfPpcn`;

const DBUri = process.env.CONNECT_DB_URI || `mongodb://localhost/DB_NAME`;

const masterPassword =
	process.env.CONNECT_MASTER_PASSWORD || `2WKDiSpfJnBxtodiDDcBw5iyJkEUXvGi`;

module.exports = {
	databaseEncryptionKey,
	DBUri,
	masterPassword,
};
