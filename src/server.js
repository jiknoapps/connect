const express = require('express');
const Websocket = require('ws');
const http = require('http');
const debug = require('../debug');
const websocketHandler = require('./websocket');
const httpHandler = require('./http');

const log = debug('startup');
const app = express();
const server = http.createServer(app);

websocketHandler(new Websocket.Server({ server }));
httpHandler(app);

const port = process.env.PORT || 3000;
server.listen(port, () => log.info(`Server is listening on port ${port}...`));
