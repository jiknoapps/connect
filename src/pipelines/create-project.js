const createConnection = require('../database/create-connection');
const createModel = require('../database/create-model');
const projectSchema = require('../schemas/config/project');
const masterPasswordIsValid = require('../services/master-password-is-valid');

module.exports = async (name, masterPassword) => {
	if (!masterPasswordIsValid(masterPassword))
		return `Invalid master password`;

	const conn = await createConnection(`config`);
	const Project = createModel(conn, `projects`, projectSchema);

	const projectWithSameName = await Project.findOne({ name });
	if (projectWithSameName) return `This project name is already taken`;

	const project = await new Project({
		name,
		databases: [],
	}).save();

	return project;
};
