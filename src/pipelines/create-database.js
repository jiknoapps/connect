const createConnection = require('../database/create-connection');
const createModel = require('../database/create-model');
const { projectSchema } = require('../schemas/config/project');
const masterPasswordIsValid = require('../services/master-password-is-valid');

module.exports = async (
	projectName,
	databaseName,
	masterPassword,
	databaseKeys
) => {
	if (!masterPasswordIsValid(masterPassword))
		return `Invalid master password`;

	const conn = await createConnection(`config`);
	const Project = createModel(conn, `projects`, projectSchema);

	let project = await Project.findOne({ name: projectName });

	if (!project) return `Invalid project`;

	if (project.databases.find((db) => db.name == databaseName))
		return `This database already exists`;

	project.databases.push({
		shortName: databaseName,
		name: `${project.name}_${databaseName}`,
		keys: databaseKeys,
		project: project.name,
		collections: [],
	});

	project.save();

	return project;
};
