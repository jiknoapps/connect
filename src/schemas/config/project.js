const { Schema } = require('mongoose');

const collectionSchema = new Schema({
	name: {
		unique: true,
		required: true,
		type: String,
	},
});

const keysSchema = new Schema({
	read: {
		required: true,
		type: String,
	},
	write: {
		required: true,
		type: String,
	},
	delete: {
		required: true,
		type: String,
	},
	all: {
		required: true,
		type: String,
	},
});

const databaseSchema = new Schema({
	shortName: {
		required: true,
		type: String,
		unique: true,
	},
	name: {
		type: String,
		required: true,
	},
	keys: {
		type: keysSchema,
		required: true,
	},
	project: {
		required: true,
		type: String,
	},
	collections: [
		{
			type: collectionSchema,
		},
	],
});

module.exports = new Schema({
	name: {
		required: true,
		type: String,
		unique: true,
	},
	databases: [
		{
			type: databaseSchema,
		},
	],
});
