const serveStatic = require('./endpoint-helpers/serve-static');

module.exports = (app) => {
	app.get('/', (req, res) => serveStatic(res, `index.html`));
	app.get('/favicon.ico', (req, res) => serveStatic(res, `favicon.ico`));
	app.get('/bug-logo.svg', (req, res) => serveStatic(res, `bug-logo.svg`));
};
