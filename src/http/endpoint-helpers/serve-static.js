const path = require('path');

module.exports = (res, file) => {
	res.sendFile(path.resolve(`src/views/${file}`));
};
