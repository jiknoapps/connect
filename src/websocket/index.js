const handleMethods = require('./handle-methods');
const prepareMessge = require('./prepare-message');

module.exports = (server) => {
	server.on('connection', (connection) => {
		connection.on('message', (message) => {
			handleMethods(prepareMessge(message));
		});
	});
};
