const debug = require('../../debug');

const log = debug('websocket');

module.exports = (rawMessage) => {
	if (typeof rawMessage !== 'string') {
		log.error(
			`Message must be a string.  Recieved type "${typeof rawMessage}".  Ignoring this message.`
		);
		return {
			method: 'ignore',
			data: null,
		};
	}

	try {
		const message = JSON.parse(rawMessage);

		return {
			method: message.method,
			data: message.data,
		};
	} catch (e) {
		log.error(
			`Error parsing message.\n\n`,
			e,
			`\n\nMessage recieved:\n${rawMessage}\nIgnoring this message.`
		);
		return {
			method: 'ignore',
			data: null,
		};
	}
};
