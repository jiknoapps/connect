const debug = require('../../debug');

const logger = debug('websocket');

module.exports = ({ method, data }) => {
	if (method === 'ignore') logger.warn(`Ignoring message: ${data}`);
};
