const mongoose = require('mongoose');
const debug = require('../../debug');
const { DBUri } = require('../constants/keys');

const logger = debug('database');
const connections = {};

const createConnection = async (database) => {
	const options = { useUnifiedTopology: true, useNewUrlParser: true };
	const uri = DBUri.replace(`DB_NAME`, database);

	try {
		const newConnection = await mongoose.createConnection(uri, options);
		connections[database] = newConnection;

		logger.info(`Connected to ${uri}...`);

		return newConnection;
	} catch (error) {
		logger.criticalError(`Connection to ${uri} failed!`, error);
	}
};

module.exports = async (database) => {
	if (connections[database]) return connections[database];
	return await createConnection(database);
};
