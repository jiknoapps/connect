const simpleEncryptor = require('simple-encryptor');
const { databaseEncryptionKey } = require('../constants/keys');

const encryptor = simpleEncryptor(databaseEncryptionKey);

module.exports.encrypt = (data) => {
	return encryptor.encrypt(data);
};
module.exports.decrypt = (data) => {
	return encryptor.decrypt(data);
};
