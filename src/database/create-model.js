const { Schema } = require('mongoose');

module.exports = (conn, collection, schema) => {
	return conn.model(
		`Model`,
		schema ||
			new Schema({
				data: {
					type: String,
					required: true,
				},
			}),
		collection
	);
};
