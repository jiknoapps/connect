const { masterPassword } = require('../constants/keys');

module.exports = (password) => {
	return password === masterPassword;
};
