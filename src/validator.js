const Joi = require('@hapi/joi');

module.exports = (schema, value) => {
	const { error } = Joi.object(schema(Joi)).validate(value);

	if (error) return error.details[0].message;
	else return null;
};
