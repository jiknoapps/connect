const createProject = require('../src/pipelines/create-project');
const debug = require('../debug');
const { masterPassword } = require('../src/constants/keys');

const logger = debug('tests');

describe(`create-project`, () => {
	it(`should return a value when params are valid`, async () => {
		const project = await createProject(
			`my_awesome_project`,
			masterPassword
		);

		logger.info(project);

		expect(project).toBeTruthy();
	});
	it(`should return an error when password is invalid`, async () => {
		const project = await createProject(
			`some_name`,
			`someinvalidmasterpassword`
		);

		logger.info(project);

		expect(project).toMatch(/invalid/i);
	});
});
