const createConnection = require('../src/database/create-connection');
const createModel = require('../src/database/create-model');
const { encrypt, decrypt } = require('../src/database/encryption');

describe(`Connection pool`, () => {
	it(`should return a connection`, async () => {
		const res = createConnection(`config`);
		expect(res).toBeTruthy();
	});
});

describe(`Model creator`, () => {
	it(`should create a hello collection in the test database for manual review`, async () => {
		const model = createModel(await createConnection('test'), `hello`);
		new model({ data: 'hi' }).save();
	});
});

describe(`Encryptor`, () => {
	it(`should return a random string when object is passed`, () => {
		const toEnctypt = {
			root: {
				posts: {},
				data: {
					something: {
						other:
							'otherotherotherotherotherotherotherotherotherotherotherotherother',
					},
				},
			},
		};
		const encrypted = encrypt(toEnctypt);

		expect(decrypt(encrypted)).toMatchObject(toEnctypt);
	});
});
